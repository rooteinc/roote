from django.shortcuts import render
from django.views import generic
from django.db.models import Q
from django.utils import timezone
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from .models import Product
# Create your views here.

class ProductListView(generic.ListView):
    model = Product
    template_name = 'products/product_list.html'
    context_object_name = 'all_products'

    def get_queryset(self):
    	return Product.objects.all()


    def get_context_data(self,*args, **kwargs):
        context = super(ProductListView, self).get_context_data(*args, **kwargs)
        context["now"] = timezone.now()
        context["query"] = self.request.GET.get("q")
        return context

    def get_queryset(self,*args, **kwargs):
        qs = super(ProductListView, self).get_queryset(*args, **kwargs)
        query = self.request.GET.get("q")
        if query:
            qs = self.model.objects.filter(
                Q(title__icontains=query)|
                Q(description__icontains=query)
            )
            try:
                qs2 = self.model.objects.filter(
                    Q(price=query)
                )
                qs = (qs|qs2).distinct()
            except:
                pass
        return qs

class ProductDetailView(generic.DetailView):
    model = Product
    template_name = 'products/product_detail.html'
