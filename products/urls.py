from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static


from .views import (
    ProductDetailView,
    ProductListView,
)
app_name = 'products'
urlpatterns = [

    #products list
    url(r'^$', ProductListView.as_view(), name='product_list'),
    #products on display
    url(r'^(?P<pk>[0-9]+)/$', ProductDetailView.as_view(), name='product_detail'),
    ]
