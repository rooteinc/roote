from django.conf.urls import url, include
from .views import (
    IndexView,
    DetailView,
    RooteCreate,
    LocationCreate,
    RooteUpdate,
    RooteLikeToggle,
    RooteDelete,
    RooteDelete,
    FoodyView,
    UrbanView,
    NightLifeView,
    OutDoorsView,
    HomeView,
    TemplateView
    )
app_name = 'post'

urlpatterns = [
    url(r'^$', IndexView.as_view(), name='index'),

    url(r'^choice/$', TemplateView.as_view(), name='choice'),

     #/post/id
    url(r'^(?P<pk>[0-9]+)/$', DetailView.as_view(), name='detail'),

    #adding rootes
    url(r'^roote/add/$', RooteCreate.as_view(), name='roote-add'),

	#adding Locations
    url(r'^location/add/$', LocationCreate.as_view(), name='location-add'),

    #updating rootes
    url(r'^(?P<pk>[0-9]+)/edit/$', RooteUpdate.as_view(), name='roote-update'),

    #like roote
    url(r'^(?P<pk>[0-9]+)/like/$', RooteLikeToggle.as_view(), name='like-toggle'),

    #deleting rootes
    url(r'^(?P<pk>[0-9]+)/delete/$', RooteDelete.as_view(), name='roote-delete'),

    #foody
    url(r'^foody/$', FoodyView.as_view(), name='foody'),

    #urban
    url(r'^urban/$', UrbanView.as_view(), name='urban'),

    #nightlife
    url(r'^nightlife/$', NightLifeView.as_view(), name='night'),

    #outdoors
    url(r'^outdoors/$', OutDoorsView.as_view(), name='outdoors'),

    # profilepage
     url(r'^home/$', HomeView.as_view(), name='home'),


]
