from django.views import generic
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.shortcuts import get_object_or_404, render, redirect
from django.core.urlresolvers import reverse_lazy
from django.views.generic import View, RedirectView, TemplateView
from .models import Roote, Location, Category
from analytics.models import UserSession
from django.contrib.auth.models import User
from .forms import LocationCreate
__all__=[Category,User,Location]
from rest_framework.response import Response
from analytics.signals import user_logged_in
from analytics.utils import get_client_city_data, get_client_ip
from django.db.models import Count


class HomeView(generic.ListView):
    model = Roote

    template_name = 'post/home.html'
    def get_queryset(self):
        return Roote.objects.filter(owner =self.request.user)


class IndexView(generic.ListView):
	template_name = 'post/index.html'
	context_object_name = 'all_post'

	def get_queryset(self):
		return Roote.objects.all().order_by('-timestamp')

class TemplateView(TemplateView):
    template_name = 'post/choice.html'


class DetailView(generic.DetailView):
    model = Roote
    template_name = 'post/detail.html'

    def get_form_kwargs(self):
    	kwargs = super(LocationCreate, self).get_form_kwargs()
    	kwargs['user'] = self.request.user
    	return kwargs
    def get_object(self, *args, **kwargs):
         pk  =self.kwargs.get('pk')
         obj = get_object_or_404(Roote, pk=pk)
         return obj
    def get_context_data( self, *args, **kwargs ):
        context =super(DetailView, self).get_context_data(*args, **kwargs)

        query = self.request.GET.get("q")
        if query:
        	qs = Roote.objects.search(query)
        return context




def post_detail(request, pk=None):
    instance = get_object_or_404(Post, pk=pk)
    if instance.updated > timezone.now().date() or instance.draft:
        if not request.user.is_staff or not request.user.is_superuser:
            raise Http404
    share_string = quote_plus(instance.content)

    initial_data = {
            "content_type": instance.get_content_type,
            "object_id": instance.id
    }

    context = {
        "title": instance.title,
        "instance": instance,
        "share_string": share_string,
    }
    return render(request, "posts/post_detail.html", context)



class RooteLikeToggle(RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        pk = self.kwargs.get("pk")
        print(pk)
        obj = get_object_or_404(Roote, pk=pk)
        url_ = obj.get_absolute_url()
        user = self.request.user
        if user.is_authenticated():
            if user in obj.likes.all():
                obj.likes.remove(user)
            else:
                obj.likes.add(user)
        return url_



class RooteCreate(CreateView):
    model = Roote
    fields = ['roote_name', 'category', 'roote_photo']


    def form_valid(self, form):
        instance = form.save(commit=False)
        instance.owner =self.request.user
        return super(RooteCreate, self).form_valid(form)



class LocationCreate(CreateView):
        model = Location
        #fields = ['roote','coordinate', 'place_name']
        success_url = reverse_lazy('post:index')
        form_class = LocationCreate

        def form_valid(self, form):
            if form.is_valid():
                roote = Roote.objects.filter(owner =self.request.user)
                instance = form.save(commit=False)
                instance.user = self.request.user
                instance.ip_address = get_client_ip(self.request)
                instance.city_data = get_client_city_data(instance.ip_address)
                if instance.city_data:
                    try:
                        instance.latitude= instance.city_data['latitude']
                    except:
                        instance.longitude = None
                    try:
                        instance.longitude= instance.city_data['longitude']
                    except:
                        instance.longitude = None
                instance.session_key = self.request.session.session_key
                user_logged_in.send(self.request.user, request=self.request)
            return super(LocationCreate, self).form_valid(form)

        def get_form_kwargs(self):
        	kwargs = super(LocationCreate, self).get_form_kwargs()
        	kwargs['user'] = self.request.user
        	return kwargs

class RooteUpdate(UpdateView):
    model = Roote
    fields = ['roote_name', 'category', 'roote_photo']



class RooteDelete(DeleteView):
    model = Roote
    success_url = reverse_lazy('post:index')

class FoodyView(generic.ListView):
    model = Roote
    template_name = 'post/foody.html'
    context_object_name = 'all_post'

    def get_queryset(self):
        return Roote.objects.filter(category__categories__startswith='FOODY').annotate(num_likes=Count('likes')).order_by('-num_likes')
class UrbanView(generic.ListView):
    model = Roote
    template_name = 'post/urban.html'
    context_object_name = 'all_post'

    def get_queryset(self):
        return Roote.objects.filter(category__categories__startswith='URBANEXPLORE').annotate(num_likes=Count('likes')).order_by('-num_likes')

class NightLifeView(generic.ListView):
    model = Roote
    template_name = 'post/nightlife.html'
    context_object_name = 'all_post'

    def get_queryset(self):
        return Roote.objects.filter(category__categories__startswith='NIGHTLIFE').annotate(num_likes=Count('likes')).order_by('-num_likes')

class OutDoorsView(generic.ListView):
    model = Roote
    template_name = 'post/outdoors.html'
    context_object_name = 'all_post'

    def get_queryset(self):
        return Roote.objects.filter(category__categories__startswith='OUTDOORS').order_by('-timestamp').annotate(num_likes=Count('likes')).order_by('-num_likes')
