from django.conf.urls import url
from django.contrib import admin
from .views import (
    RooteListAPIView,
    RooteCreateAPIView,
    RooteDetailAPIView,
    RooteUpdateAPIView,
    RooteDeleteAPIView,
    RooteLikeAPIToggle,
    LocationListAPIView,
)


urlpatterns = [
    url(r'^$', RooteListAPIView.as_view(), name='list'),
    url(r'^(?P<pk>[0-9]+)/$', RooteDetailAPIView.as_view(), name='detail'),
    url(r'^(?P<pk>[0-9]+)/edit/$', RooteUpdateAPIView.as_view(), name='update'),
    url(r'^(?P<pk>[0-9]+)/delete/$', RooteDeleteAPIView.as_view(), name='delete'),
    url(r'^(?P<pk>[0-9]+)/like/$', RooteLikeAPIToggle.as_view(), name='like-api-toggle'),
    url(r'create/$', RooteCreateAPIView.as_view(), name='create'),
    url(r'^location/$', LocationListAPIView.as_view(), name='location-api-list'),
    ]
