from django.shortcuts import get_object_or_404
from django.db.models import Q
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import authentication, permissions

from rest_framework.generics import (
     CreateAPIView,
     ListAPIView,
     RetrieveAPIView,
     DestroyAPIView,
     UpdateAPIView,
     )

from .permissions import isOwnerOrReadOnly


from rest_framework.filters import (
    SearchFilter,
    OrderingFilter,
    )

from rest_framework.permissions import (
    AllowAny,
    IsAuthenticated,
    IsAdminUser,
    IsAuthenticatedOrReadOnly,

    )
from post.models import Roote, Location
from .serializers import (
        RooteListSerializer,
        RooteDetailSerializer,
        RooteCreateUpdateSerializer,
        LocationSerializer,
        )



class RooteLikeAPIToggle(APIView):
    authentication_classes = (authentication.SessionAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, pk=None, format=None):
        obj = get_object_or_404(Roote, pk=pk)
        url_ = obj.get_absolute_url()
        user = self.request.user
        updated = False
        liked = False
        if user.is_authenticated():
            if user in obj.likes.all():
                liked = False
                obj.likes.remove(user)
            else:
                liked = True
                obj.likes.add(user)
            updated = True
        data = {
            "updated": updated,
            "liked": liked
        }
        return Response(data)



class RooteCreateAPIView(CreateAPIView):
    queryset = Roote.objects.all()
    serializer_class = RooteCreateUpdateSerializer

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)
        permissions_classes = [IsAuthenticated]



class RooteListAPIView(ListAPIView):
    serializer_class = RooteListSerializer
    #Filtering through the ?search=&ordering
    filter_backends = [SearchFilter, OrderingFilter]
    search_fields = ['roote_name']
    #Filtering through the ?q=
    def get_queryset(self, *args, **kwargs):
        queryset_list = Roote.objects.all()
        query = self.request.GET.get("q")
        if query:
            queryset_list = queryset_list.filter(
                    Q(roote_name__icontains=query)
                    ).distinct()
        return queryset_list


class RooteDetailAPIView(RetrieveAPIView):
    queryset = Roote.objects.all()
    serializer_class = RooteDetailSerializer

class RooteUpdateAPIView(UpdateAPIView):
    queryset = Roote.objects.all()
    serializer_class = RooteCreateUpdateSerializer
    permission_classes = [IsAuthenticatedOrReadOnly,isOwnerOrReadOnly,IsAdminUser]
    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


class RooteDeleteAPIView(DestroyAPIView):
    queryset = Roote.objects.all()
    serializer_class = RooteDetailSerializer




class LocationListAPIView(ListAPIView):
    serializer_class = LocationSerializer
    def get_queryset(self, *args, **kwargs):
        queryset_list = Location.objects.all()
        query = self.request.GET.get("q")
        if query:
            queryset_list = queryset_list.filter(
                    Q(place_name__icontains=query)
                    ).distinct()
        return queryset_list
