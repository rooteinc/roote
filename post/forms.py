from .models import  Location
from .models import  Roote
from django import forms


class LocationCreate(forms.ModelForm):

    class Meta:
        model = Location
        fields = [
			'roote',
            'coordinate',
            'place_name'
        ]
    def __init__(self, user=None, *args, **kwargs):
        self.user = user
        super(LocationCreate, self).__init__(*args, **kwargs)
        self.fields['roote'].queryset = Roote.objects.filter(owner=user)
