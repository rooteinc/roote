from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from website import views



urlpatterns = [
    url(r'^$', views.index_redirect,  name='index_redirect'),
    url(r'^admin/', admin.site.urls),
    url(r'^post/', include('post.urls', namespace='post')),
    url(r'^products/', include('products.urls', namespace='products')),
    url(r'^accounts/', include('accounts.urls', namespace='accounts')),
    url(r'^api/post/', include("post.api.urls", namespace='post-api')),
    url(r'^analytics/', include('analytics.urls', namespace='analytics')),


    ]







if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
