from django.contrib import admin
from accounts.models import UserProfile
# Register your models here.


class UserProfileAdmin(admin.ModelAdmin):
    list_display = ('user', 'cityfrom', 'bio')
     #grabs the list and displays it on the admin
    def cityfrom(self, obj):
        return obj.city

    def get_queryset(self, request):
        queryset =super(UserProfileAdmin, self).get_queryset(request)
        queryset =  queryset.order_by('user')
        return queryset


admin.site.register(UserProfile,UserProfileAdmin)
