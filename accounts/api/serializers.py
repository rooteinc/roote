from django.contrib.auth import get_user_model
from accounts.models import UserProfile

from rest_framework.serializers import  (
    ModelSerializer,
    HyperlinkedIdentityField,
    SerializerMethodField,
    )
User = get_user_model()

class UserDisplaySerializer(ModelSerializer):
    class Meta:
        model= User
        fields = [
        'username',
        'first_name',
        'last_name',
        ]
