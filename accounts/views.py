
from django.shortcuts import render, redirect
from django.urls import reverse
from django.views import generic
from accounts.forms import (
    RegistrationForm,
    EditProfileForm
)

from django.contrib.auth.models import User
from django.contrib.auth.forms import UserChangeForm, PasswordChangeForm
from django.contrib.auth import update_session_auth_hash,authenticate,login
from django.shortcuts import get_object_or_404
from post.models import Roote

__all__ = [User, UserChangeForm]



def login_view(request):
    print ( request.user.is_authenticated () )
    title = "Login"
    form = UserLoginForm(request.POST or None)
    if form.is_valid():
        username = form.cleaned_data.get("username")
        password = form.cleaned_data.get("password")
        user = authenticate(username=username,password=password)
        login(request, user)
        print(request.user.is_authenticated())


        if user.is_active:

                    login(request, user)
                    return redirect('accounts:view_profile')


        __all__ = (username,password,user)
    return render(request, "form.html", {"form":form,"title":title})


def register(request):
    if request.method =='POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            form.save()
            username= request.POST.get('username')
            password = request.POST.get('password1')
            user = authenticate(
                request,
                username=username,
                password=password,
            )
            login(request, user)

            return redirect(reverse('accounts:view_profile'))
    else:
        form = RegistrationForm()

        args = {'form': form}
        return render(request, 'accounts/reg_form.html', args)



def logout_view(request):
    title = "Log out Success"
    logout(request)
    return redirect('accounts:login')


class ViewProfile(generic.ListView):
    model = Roote
    template_name = 'accounts/profile.html'
    def get(self, request, *args, **kwargs):
        pk = kwargs.get('pk')
        owner = get_object_or_404(User, pk=pk) if pk else request.user  # <<<

        users = User.objects.exclude(id=request.user.id)[:]
        object_list = Roote.objects.filter(owner=owner).order_by('-timestamp')
        context = {'object_list': object_list, 'users': users,'user': owner}

        return render(request, self.template_name, context)


def edit_profile(request):
    if request.method =='POST':
        form = EditProfileForm(request.POST, instance=request.user)

        if form.is_valid():
            form.save()
            return redirect(reverse('accounts:view_profile'))
    else:
        form= EditProfileForm(instance=request.user)
        kwargs = {'form': form}
        return render(request,'accounts/edit_profile.html', kwargs)


def change_password(request):
    if request.method=='POST':
        form= PasswordChangeForm(data=request.POST, user=request.user)

        if form.is_valid():
            form.save()
            update_session_auth_hash(request, form.user)
            return redirect('accounts:view_profile')
        else:
            return redirect('change-password')
    else:
        form= PasswordChangeForm(user=request.user)
        kwargs = {'form': form}
        return render(request,'change-password.html', kwargs)
